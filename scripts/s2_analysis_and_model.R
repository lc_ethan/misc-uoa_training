
#### Section 0: Session Setup ----
library(checkpoint)
checkpoint(
  snapshotDate = "2020-10-01"
  , R.version = "3.6.1" # replaced with your version of R
  , checkpointLocation = "." # replaced with your own directory
)

library(tidyverse)
library(lubridate)
library(zoo)
library(Metrics)
library(tidymodels)

rm(list = ls()); gc()

## ggplot config ----
theme_set(
  theme_bw(base_size = 20)
)

## directories ----
dir.data_raw <- "data/raw/sales"
dir.data_proc <- "data/proc"

dir.src <- "scripts"

## utility functions ----
source(file.path(dir.src, "utility_functions.r"))

#### Section 1: Data Import ----
load(file.path(dir.data_proc, "sales_dim.rda"))

#### Section 2: EDA ----
# base data creation 
df.base_month <- df.sales_master %>% 
  select(BASE_DATE, QTY, SALES) %>% 
  group_by(BASE_DATE) %>%
  summarise_if(is.numeric, sum, na.rm = TRUE) %>% 
  ungroup()

## trend ----
# exponential growth
df.base_month %>%
  arrange(BASE_DATE) %>% 
  mutate(MA12 = rollmean(QTY, 12, fill = NA, align = "right")) %>%
  transmute(
    BASE_DATE
    , `Actual` = QTY
    , MA12
  ) %>% 
  gather(METRIC, VALUE, -BASE_DATE) %>% 
  ggplot(aes(x = BASE_DATE)) +
  geom_line(aes(y = VALUE, color = METRIC), size = 1) +
  xlab("Month") +
  ylab("Sales Quantity (#)") +
  guides(color = guide_legend("Metrics"))
  # theme(legend.position = "top")

## seasonality ----
# irregular seasonality
df.base_month %>% 
  filter(year(BASE_DATE) < 2020 & year(BASE_DATE) > 2015) %>% 
  mutate(BASE_MONTH = format(BASE_DATE, "%m")) %>% 
  mutate(BASE_YEAR = as.character(year(BASE_DATE))) %>% 
  group_by(BASE_YEAR) %>% 
  mutate(PC = QTY/sum(QTY)) %>% 
  ggplot() +
  geom_bar(aes(x = BASE_MONTH, y = PC, fill = BASE_YEAR), stat = "identity", position = "dodge") +
  scale_y_continuous(labels = scales::percent) +
  # theme_bw(base_size = 20) +
  xlab("Month") +
  ylab("Sales Quantity by Year (%)") + 
  guides(fill = guide_legend("Year"))
  
## checking effects toward sales ----
# only time is significant correlated with sales quantity
fit.effects <- df.base_month %>% 
  mutate(
    MONTH = format(BASE_DATE, '%b')
    , FLAG_2019 = ifelse(BASE_DATE >= as.Date("2019-01-01"), 'Y', 'N')
  ) %>% 
  lm(log(SALES) ~ BASE_DATE + MONTH + FLAG_2019, data = .)

# lm summary
summary(fit.effects)

# anova
anova(fit.effects)


## check seasonal aggregates ----
df.base_season <- df.sales_master %>%
  select(BASE_SEASON, QTY, SALES) %>% 
  group_by(BASE_SEASON) %>%
  summarise_if(is.numeric, sum, na.rm = TRUE) %>% 
  ungroup()

# irregular seasonality at monthly level is diluted at seasonal level
df.base_season %>%  
  ggplot(aes(x = BASE_SEASON)) +
  geom_line(aes(y = QTY), size = 1) +
  xlab("Season") +
  ylab("Sales Quantity (#)")

#### Section 3: Forecast Model ----
## create model input ----
df.mod <- df.sales_master %>% 
  transmute(
    TIME = BASE_SEASON
    , Y = QTY
  ) %>% 
  group_by(TIME) %>% 
  summarise(Y = sum(Y)) %>% 
  ungroup()

## split model data by training/testing sets ----
ls.mod <- initial_time_split(df.mod, prop = 3/4)
df.mod_train <- ls.mod %>% 
  training() %>% 
  mutate(DATA_TYPE = "Train")

df.mod_test <- ls.mod %>% 
  testing() %>% 
  mutate(DATA_TYPE = "Test")

## model fitting ----
fit.exp <- lm(log(Y) ~ TIME, data = df.mod_train)
summary(fit.exp)

## checking accuracy ----
df.accuracy_check <- df.mod_test %>% 
  mutate(Y_FIT = exp(predict(fit.exp, .))) %>%
  summarise(
    RMSE = Metrics::rmse(Y, Y_FIT)
    , MAPE = Metrics::mape(Y, Y_FIT)
  )
  
df.accuracy_check

df.mod_train %>% 
  bind_rows(df.mod_test) %>% 
  mutate(Y_FIT = exp(predict(fit.exp, .))) %>% 
  ggplot(aes(x = TIME)) +
  geom_line(aes(y = Y_FIT), color = "black", size = 1) +
  geom_point(aes(y = Y, color = DATA_TYPE), size = 5) +
  xlab("Season") +
  ylab("Sales Quantity Actual and Forecast") +
  guides(color = guide_legend("Data"))

## prediction ----
df.pred_input <- as.Date("2015-04-01") %>% 
  seq.Date(as.Date("2021-12-01"), by = "month") %>% 
  floor_date("3 month") %>% 
  unique() %>% 
  data.frame(
    TIME = .
  ) %>% 
  tbl_df()

df.pred_95 <- df.pred_input %>% 
  predict(fit.exp, newdata = ., interval = "prediction") %>%
  tbl_df() %>% 
  transmute(
    UPR_95 = exp(upr)
    , LWR_05 = exp(lwr)
  )

df.pred_75 <- df.pred_input %>% 
  predict(fit.exp, newdata = ., interval = "prediction", level = 0.75) %>%
  tbl_df() %>% 
  transmute(
    UPR_75 = exp(upr)
    , LWR_25 = exp(lwr)
  )

df.sales_pred <- df.pred_input %>% 
  mutate(FIT = exp(predict(fit.exp, .))) %>% 
  bind_cols(df.pred_75) %>% 
  bind_cols(df.pred_95) %>% 
  left_join(df.mod) %>% 
  mutate(DATA_TYPE = ifelse(is.na(Y), "Forecast", "Actual")) %>% 
  mutate(Y = ifelse(DATA_TYPE %in% "Actual", Y, FIT))


df.sales_pred %>% 
  ggplot(aes(x = TIME)) +
  geom_ribbon(aes(ymin = LWR_05, ymax = UPR_95), fill = "red", alpha = 0.3) +
  geom_ribbon(aes(ymin = LWR_25, ymax = UPR_75), fill = "yellow", alpha = 0.6) +
  geom_line(aes(y = FIT), size = 1, color = "black") +
  geom_point(aes(y = Y, color = DATA_TYPE), size = 5) +
  xlab("Season") +
  ylab("Sales Quantity Forecast") +
  guides(color = guide_legend("Data"))

  
    
    
    


  







