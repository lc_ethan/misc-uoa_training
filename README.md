# UoA Data Science Workshop

## About this workshop

This is a hands-on workshop for best practice Data Science in the real world. It also provides an opportunities for students to talk to Data Scientists from industries for tips and tricks.

## Business Problem

### Background
Business A is a successful retailer and they are growing rapidly. Their biggest pain point these couple years is that they sold out of large portions of their seasonal stocks on day 1. This drives missed opportunities to sell product over the course of the season. At the same time, cashflow is very critical in Business A’s industry, risky over-order can easily drive the business into insolvency, thus Business A is being very conservative regarding inventory management.

### Objective
Advise how Business A can realise its full revenue potential consistently in a low risk manner

## Glossary

### AWS S3
Access key:AKIAYVEHASLDNJUNVU3W

Secret Access Key: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

### Dropbox
https://www.dropbox.com/sh/5dq5e5xd4px30hy/AAC1gzwSuSVn5JooiMU5gtuoa?dl=0

### Data Definition
- Date – Time key
- Category – Product category (e.g. Apple, Samsung, Huawei and etc.)
- Product – Product name (e.g. iPhone 12, iPhone 11, iPhone 10 and etc.)
- Sales Quantity – Sales quantity for the product
- Gross Sales – Total sales revenue for the product

### Useful R Libraries
checkpoint, aws.s3, tidyverse, lubridate, zoo, tidymodels, Metrics

